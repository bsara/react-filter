# 2.0.1

- **[BREAKING CHANGE]** Changed `react` and `prop-types` to be peer dependencies instead
  of normal dependencies.
- **[ADDED]** Support for `object-hash` >=1.3.0 or 2.x.
- Update some dev dependencies.



# 1.3.0

- **[ADDED]** Ability to pass function as `children` instead of using `render` prop.



# 1.2.0

- **[ADDED]** Ability to skip filtering and just pass through given `items` via the new
  `skip` prop.



# 1.1.1

- Optimized package size by only including what is needed in npm package.



# 1.1.0

- **[ADDED]** "Memoized-One" filtering
- **[REMOVED]** Component is no longer a "Pure" component. *(this is no longer needed in
  light of the memoization added and allows for rendering of child components which may
  require a re-render even though filtering does not need to be re-run)*.
- **[REMOVED]** `predicate` is no longer a required prop. Not specifying `predicate` or
  providing a `predicate` that is `null` or `undefined` will cause filtering to be skipped
  and the `items` provided will just be forwarded on to the `render` function.



# 1.0.2

- Made license notices smaller.



# 1.0.1

- Moved repository to GitLab.com
- Updated dependencies



# 1.0.0

- Initial release.
