'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _objectHash = require('object-hash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } // eslint-disable-next-line spaced-comment
/**!
 * @bsara/react-filter v2.0.2
 *
 * Copyright (c) 2019, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/react-filter/blob/master/LICENSE)
 */

var Filter = function (_React$Component) {
  _inherits(Filter, _React$Component);

  function Filter(props) {
    var _ref;

    _classCallCheck(this, Filter);

    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var _this = _possibleConstructorReturn(this, (_ref = Filter.__proto__ || Object.getPrototypeOf(Filter)).call.apply(_ref, [this, props].concat(args)));

    _this._lastItems = undefined;
    _this._lastPredicate = undefined;
    _this._lastPredicateArg = undefined;
    _this._lastPredicateArgHash = undefined;

    _this._cachedFilteredItems = undefined;
    return _this;
  }

  _createClass(Filter, [{
    key: 'render',
    value: function render() {
      var renderFunc = this.props.children != null ? this.props.children : this.props.render;

      return renderFunc(this._filterItems());
    }

    // region Private Helpers

    /** @private */

  }, {
    key: '_filterItems',
    value: function _filterItems() {
      if (this.props.skip) {
        return this.props.items;
      }

      var predicateArgHash = this.props.predicateArg == null ? undefined : (0, _objectHash.MD5)(this.props.predicateArg);

      if (this.props.items !== this._lastItems || this.props.predicate !== this._lastPredicate || this.props.predicateArg !== this._lastPredicateArg && (this._lastPredicateArgHash == null || predicateArgHash !== this._lastPredicateArgHash)) {
        this._setCachedFilteredItems();

        this._lastItems = this.props.items;
        this._lastPredicate = this.props.predicate;
        this._lastPredicateArg = this.props.predicateArg;
        this._lastPredicateArgHash = predicateArgHash;
      }

      return this._cachedFilteredItems;
    }

    /** @private */

  }, {
    key: '_setCachedFilteredItems',
    value: function _setCachedFilteredItems() {
      if (this.props.items == null || !this.props.items.length || this.props.predicate == null) {
        this._cachedFilteredItems = this.props.items;
        return;
      }

      this._cachedFilteredItems = _filter(this.props.items, this.props.predicate, this.props.predicateArg);
    }

    // endregion

  }]);

  return Filter;
}(_react2.default.Component);

exports.default = Filter;


Filter.propTypes = {
  items: _propTypes2.default.array,

  render: _propTypes2.default.func,
  children: _propTypes2.default.func,

  predicate: _propTypes2.default.func,
  predicateArg: _propTypes2.default.any,

  skip: _propTypes2.default.bool
};

/* eslint-disable valid-jsdoc */

// region Private Helpers

/** @private */
function _filter(items, predicate, predicateArg) {
  var ret = [];

  for (var i = 0; i < items.length; i++) {
    var item = items[i];

    if (predicate(item, i, predicateArg)) {
      ret.push(item);
    }
  }

  return ret;
}

// endregion

//# sourceMappingURL=index.es5.js.map