// eslint-disable-next-line spaced-comment
/**!
 * @bsara/react-filter v2.0.2
 *
 * Copyright (c) 2019, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/react-filter/blob/master/LICENSE)
 */

import React from 'react';
import PropTypes from 'prop-types';

import { MD5 } from 'object-hash';



export default class Filter extends React.Component {

  constructor(props, ...args) {
    super(props, ...args);

    this._lastItems            = undefined;
    this._lastPredicate        = undefined;
    this._lastPredicateArg     = undefined;
    this._lastPredicateArgHash = undefined;

    this._cachedFilteredItems = undefined;
  }



  render() {
    const renderFunc = (
      (this.props.children != null)
        ? this.props.children
        : this.props.render
    );

    return renderFunc(this._filterItems());
  }


  // region Private Helpers

  /** @private */
  _filterItems() {
    if (this.props.skip) {
      return this.props.items;
    }

    const predicateArgHash = ((this.props.predicateArg == null) ? undefined : MD5(this.props.predicateArg));

    if (this.props.items !== this._lastItems
        || this.props.predicate !== this._lastPredicate
        || (this.props.predicateArg !== this._lastPredicateArg && (this._lastPredicateArgHash == null || predicateArgHash !== this._lastPredicateArgHash))) {
      this._setCachedFilteredItems();

      this._lastItems            = this.props.items;
      this._lastPredicate        = this.props.predicate;
      this._lastPredicateArg     = this.props.predicateArg;
      this._lastPredicateArgHash = predicateArgHash;
    }

    return this._cachedFilteredItems;
  }


  /** @private */
  _setCachedFilteredItems() {
    if (this.props.items == null || !this.props.items.length || this.props.predicate == null) {
      this._cachedFilteredItems = this.props.items;
      return;
    }

    this._cachedFilteredItems = _filter(this.props.items, this.props.predicate, this.props.predicateArg);
  }


  // endregion
}


Filter.propTypes = {
  items: PropTypes.array,

  render:   PropTypes.func,
  children: PropTypes.func,

  predicate:    PropTypes.func,
  predicateArg: PropTypes.any,

  skip: PropTypes.bool
};



/* eslint-disable valid-jsdoc */



// region Private Helpers

/** @private */
function _filter(items, predicate, predicateArg) {
  const ret = [];

  for (let i = 0; i < items.length; i++) {
    const item = items[i];

    if (predicate(item, i, predicateArg)) {
      ret.push(item);
    }
  }

  return ret;
}

// endregion
